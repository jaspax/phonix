# Developer Guide

## Build requirements

Phonix is built on Linux using the Mono framework implementation of .NET. The following
instructions assume that you have a Debian-like system (I use Ubuntu Server, personally).

The Phonix uses the usual suite of Linux and GNU dev utils, which are standard on almost any
distribution of Linux or POSIX these days. Aside from those, you need the following:

* A Java Runtime v. 1.5 or greater
* The Mono .NET runtime and build tools, including gmcs
* The NUnit framework for running unit tests

If you're running some variant of debian, then you should install all of the following:

    debhelper (>= 7.0.50~)
    mono-devel (>= 2.10.0)
    default-jre
    nunit-console (= 2.6)
    texinfo

## Building and installing

Once you have all of the requirements, building is done with a standard Makefile. Yeah, it's
`make`. We do things old-skool around here; no fancy `.csproj` files for us. Fortunately our
build is very simple, and the available targets are pretty straightforward.

    make 
    make test 
    sudo make install

Note that the "install" target is kind of shaky right now, and might fail if you have an
unusual system configuration. Also note that the relative paths in the Makefile all assume
that you are building from the root of the repository. Running "make" in any subdirectory is
not supported.  Since the directory structure is currently very flat, this is rarely a
problem.

Here are a few other makefile targets that you might want to use:

`make doc` - build the documentation files

`make deb` - build the .deb files for installing packages onto debian

`make teste2e` - build the installation package, install it, then run a test suite that
actually invokes the compiled and installed binary. (This may run 1-2 minutes and require
you to require a sudoer password.)

There are other targets that you can find in the Makefile, but you rarely need to use any of
them directly.  

# Release Checklist

1. Update `Resx\version` to the current version number
2. Ensure that all issues slated for the release are fixed
3. Update docs in the wiki
4. Run through the release process below

## Release Process

Confirm there are no changes in the working copy.

Run all of the following, and ensure that there are no errors:

    make test
    make teste2e
    make doc
    make zip
    make deb

Create a git tag like so, filling in $VERSION and $MESSAGE as appropriate:

    git tag -a $VERSION -m $MESSAGE

Push the tag to GitLab.

    git push origin $VERSION

On GitLab, modify the release notes and add the .zip, .pdf, and .html files for
the current release.
