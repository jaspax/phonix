# About Phonix

**Phonix** is a domain-specific language for modeling phonological transformations, both
synchronic linguistic processes and diachronic sound changes. It uses a simple notation to
represent phonological rules that is very similar to the standard notation used in
linguistic texts. Here's a sample from the Romanian example included in Phonix:

    import std.features 
    import std.symbols

    # 
    # Romanian palatalization rules
    #

    # Palatalize velars before front vowels
    rule palatalize-velars
    [+cons +hi] => [-ant +dist +dr *hi] / _ [+fr]

    # Palatalize dental fricatives before high front vowels
    rule palatalize-dentals
    [+cons +ant +cont -son] => [-ant +dist] / _ [+hi +fr]

Phonix supports all of the following features and more:

* Rules based on distinctive features, not string matching
* Built-in distinctive feature set based on standard linguistic literature
* Built-in symbol sets for both X-SAMPA and Unicode IPA
* Easy extensions for adding your own features or symbols
* Multi-character symbols (digraphs)
* Diacritic symbols (accent marks and Unicode combining characters)
* Persistent rules for maintaining syllable structures or stress rules
* A debugging mode that shows you exactly what your sound changes are doing

The Phonix interpreter is a command-line program designed for easy interoperability with
other common scripting tools. You invoke in on the command line like this:

    phonix language.phonix -i input_wordlist.txt -o output_wordlist.txt

The Phonix interpreter is written in C# and can run on any platform that has .NET, including
all versions of Windows, and most flavors of Linux and MacOSX thanks to the Mono project.
Phonix development is done using Mono in a Linux environment.

# Installation

[Installing Phonix on Linux](/jaspax/phonix/wikis/linux-installation)

[Installing Phonix on Windows](/jaspax/phonix/wikis/windows-installation)

[Installing Phonix on Mac](/jaspax/phonix/wikis/mac-installation)

